package com.example.Test2SpringBootExample35Devtiro.mappers;

public interface Mapper<A, B> {
    B mapTo(A a);

    A mapFrom(B b);
}
