package com.example.Test2SpringBootExample35Devtiro;

import lombok.extern.java.Log;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Log
public class Test2SpringBootExample35DevtiroApplication {

	public static void main(String[] args) {
		SpringApplication.run(Test2SpringBootExample35DevtiroApplication.class, args);
	}

}
