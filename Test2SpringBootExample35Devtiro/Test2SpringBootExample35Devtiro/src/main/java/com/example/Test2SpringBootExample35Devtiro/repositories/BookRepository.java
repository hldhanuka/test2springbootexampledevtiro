package com.example.Test2SpringBootExample35Devtiro.repositories;

import com.example.Test2SpringBootExample35Devtiro.domain.entities.BookEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepository extends CrudRepository<BookEntity, String> {
}
