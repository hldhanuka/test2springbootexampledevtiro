package com.example.Test2SpringBootExample35Devtiro;

import com.example.Test2SpringBootExample35Devtiro.domain.dto.AuthorDto;
import com.example.Test2SpringBootExample35Devtiro.domain.dto.BookDto;
import com.example.Test2SpringBootExample35Devtiro.domain.entities.AuthorEntity;
import com.example.Test2SpringBootExample35Devtiro.domain.entities.BookEntity;

public final class TestDataUtil {
    private TestDataUtil() {

    }

    public static AuthorEntity createTestAuthorEntityA() {
        return AuthorEntity.builder()
                .id(1L)
                .name("Dhanuka 1")
                .age(25)
                .build();
    }

    public static AuthorDto createTestAuthorDtoA() {
        return AuthorDto.builder()
                .id(1L)
                .name("Dhanuka 1")
                .age(25)
                .build();
    }

    public static AuthorEntity createTestAuthorB() {
        return AuthorEntity.builder()
                .id(2L)
                .name("Dhanuka 2")
                .age(25)
                .build();
    }

    public static AuthorEntity createTestAuthorC() {
        return AuthorEntity.builder()
                .id(3L)
                .name("Dhanuka 3")
                .age(25)
                .build();
    }

    public static BookEntity createTestBookEntityA(final AuthorEntity authorEntity) {
        return BookEntity.builder()
                .isbn("573573-453503-3533")
                .title("test A")
                .authorEntity(authorEntity)
                .build();
    }

    public static BookDto createTestBookDtoA(final AuthorDto authorDto) {
        return BookDto.builder()
                .isbn("573573-453503-3533")
                .title("test A")
                .author(authorDto)
                .build();
    }

    public static BookEntity createTestBookB(final AuthorEntity authorEntity) {
        return BookEntity.builder()
                .isbn("573573-453503-3532")
                .title("test B")
                .authorEntity(authorEntity)
                .build();
    }

    public static BookEntity createTestBookC(final AuthorEntity authorEntity) {
        return BookEntity.builder()
                .isbn("573573-453503-3534")
                .title("test C")
                .authorEntity(authorEntity)
                .build();
    }
}
