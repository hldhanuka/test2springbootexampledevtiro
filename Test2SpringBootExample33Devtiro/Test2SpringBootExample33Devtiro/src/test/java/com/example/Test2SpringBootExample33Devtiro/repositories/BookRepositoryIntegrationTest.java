package com.example.Test2SpringBootExample33Devtiro.repositories;

import com.example.Test2SpringBootExample33Devtiro.TestDataUtil;
import com.example.Test2SpringBootExample33Devtiro.domain.Author;
import com.example.Test2SpringBootExample33Devtiro.domain.Book;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class BookRepositoryIntegrationTest {
    private AuthorRepository authorDao;

    private BookRepository underTest;

    @Autowired
    public BookRepositoryIntegrationTest(
            BookRepository underTest,
            AuthorRepository authorDao
    ) {
        this.underTest = underTest;
        this.authorDao = authorDao;
    }

    @Test
    public void testThatAuthorCanBeCreatedAndRecalled() {
        Author author = TestDataUtil.createTestAuthorA();

        Book book = TestDataUtil.createTestBookA(author);

        underTest.save(book);

        Optional<Book> result = underTest.findById(book.getIsbn());

        assertThat(result).isPresent();

        assertThat(result.get()).isEqualTo(book);
    }

    @Test
    public void testThatMultipleBooksCanBeCreatedAndRecalled() {
        Author authorA = TestDataUtil.createTestAuthorA();

        Book bookA = TestDataUtil.createTestBookA(authorA);
        underTest.save(bookA);

        Book bookB = TestDataUtil.createTestBookB(authorA);
        underTest.save(bookB);

        Book bookC = TestDataUtil.createTestBookC(authorA);
        underTest.save(bookC);

        Iterable<Book> results = underTest.findAll();

        assertThat(results)
                .hasSize(3)
                .containsExactly(bookA, bookB, bookC);
    }

    @Test
    public void testThatAuthorsCanBeUpdated() {
        Author authorA = TestDataUtil.createTestAuthorA();

        Book bookA = TestDataUtil.createTestBookA(authorA);
        bookA.setAuthor(authorA);

        underTest.save(bookA);

        bookA.setTitle("UPDATE");

        underTest.save(bookA);

        Optional<Book> result = underTest.findById(bookA.getIsbn());

        assertThat(result).isPresent();

        assertThat(result.get()).isEqualTo(bookA);
    }

    @Test
    public void testThatAuthorsCanBeDeleted() {
        Author authorA = TestDataUtil.createTestAuthorA();
        authorDao.save(authorA);

        Book bookA = TestDataUtil.createTestBookA(authorA);

        underTest.save(bookA);

        underTest.deleteById(bookA.getIsbn());

        Optional<Book> result = underTest.findById(bookA.getIsbn());

        assertThat(result).isEmpty();
    }
}
