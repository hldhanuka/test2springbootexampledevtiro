package com.example.Test2SpringBootExample33Devtiro;

import com.example.Test2SpringBootExample33Devtiro.domain.Author;
import com.example.Test2SpringBootExample33Devtiro.domain.Book;

public final class TestDataUtil {
    private TestDataUtil() {

    }

    public static Author createTestAuthorA() {
        return Author.builder()
                .id(1L)
                .name("Dhanuka 1")
                .age(25)
                .build();
    }

    public static Author createTestAuthorB() {
        return Author.builder()
                .id(2L)
                .name("Dhanuka 2")
                .age(25)
                .build();
    }

    public static Author createTestAuthorC() {
        return Author.builder()
                .id(3L)
                .name("Dhanuka 3")
                .age(25)
                .build();
    }

    public static Book createTestBookA(final Author author) {
        return Book.builder()
                .isbn("573573-453503-3533")
                .title("test A")
                .author(author)
                .build();
    }

    public static Book createTestBookB(final Author author) {
        return Book.builder()
                .isbn("573573-453503-3532")
                .title("test B")
                .author(author)
                .build();
    }

    public static Book createTestBookC(final Author author) {
        return Book.builder()
                .isbn("573573-453503-3534")
                .title("test C")
                .author(author)
                .build();
    }
}
