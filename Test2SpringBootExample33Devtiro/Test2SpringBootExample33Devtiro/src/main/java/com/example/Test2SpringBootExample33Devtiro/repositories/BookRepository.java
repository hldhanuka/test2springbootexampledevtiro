package com.example.Test2SpringBootExample33Devtiro.repositories;

import com.example.Test2SpringBootExample33Devtiro.domain.Book;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepository extends CrudRepository<Book, String> {
}
