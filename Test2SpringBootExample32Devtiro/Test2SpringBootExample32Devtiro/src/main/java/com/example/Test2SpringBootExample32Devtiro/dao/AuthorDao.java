package com.example.Test2SpringBootExample32Devtiro.dao;

import com.example.Test2SpringBootExample32Devtiro.domain.Author;

import java.util.List;
import java.util.Optional;

public interface AuthorDao {
    void create(Author author);

    Optional<Author> fineOne(long l);

    List<Author> find();

    void update(long id, Author author);

    void delete(long id);
}
