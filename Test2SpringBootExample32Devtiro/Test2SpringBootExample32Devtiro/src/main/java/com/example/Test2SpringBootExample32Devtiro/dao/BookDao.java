package com.example.Test2SpringBootExample32Devtiro.dao;

import com.example.Test2SpringBootExample32Devtiro.domain.Book;

import java.util.List;
import java.util.Optional;

public interface BookDao {
    void create(Book book);

    Optional<Book> fineOne(String isbn);

    List<Book> find();

    void update(String id, Book book);

    void delete(String isbn);
}
