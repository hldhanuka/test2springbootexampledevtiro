package com.example.Test2SpringBootExample36Devtiro.repositories;

import com.example.Test2SpringBootExample36Devtiro.TestDataUtil;
import com.example.Test2SpringBootExample36Devtiro.domain.entities.AuthorEntity;
import com.example.Test2SpringBootExample36Devtiro.domain.entities.BookEntity;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class BookEntityRepositoryIntegrationTest {
    private AuthorRepository authorDao;

    private BookRepository underTest;

    @Autowired
    public BookEntityRepositoryIntegrationTest(
            BookRepository underTest,
            AuthorRepository authorDao
    ) {
        this.underTest = underTest;
        this.authorDao = authorDao;
    }

    @Test
    public void testThatAuthorCanBeCreatedAndRecalled() {
        AuthorEntity authorEntity = TestDataUtil.createTestAuthorEntityA();

        BookEntity bookEntity = TestDataUtil.createTestBookEntityA(authorEntity);

        underTest.save(bookEntity);

        Optional<BookEntity> result = underTest.findById(bookEntity.getIsbn());

        assertThat(result).isPresent();

        assertThat(result.get()).isEqualTo(bookEntity);
    }

    @Test
    public void testThatMultipleBooksCanBeCreatedAndRecalled() {
        AuthorEntity authorEntityA = TestDataUtil.createTestAuthorEntityA();

        BookEntity bookEntityA = TestDataUtil.createTestBookEntityA(authorEntityA);
        underTest.save(bookEntityA);

        BookEntity bookEntityB = TestDataUtil.createTestBookB(authorEntityA);
        underTest.save(bookEntityB);

        BookEntity bookEntityC = TestDataUtil.createTestBookC(authorEntityA);
        underTest.save(bookEntityC);

        Iterable<BookEntity> results = underTest.findAll();

        assertThat(results)
                .hasSize(3)
                .containsExactly(bookEntityA, bookEntityB, bookEntityC);
    }

    @Test
    public void testThatAuthorsCanBeUpdated() {
        AuthorEntity authorEntityA = TestDataUtil.createTestAuthorEntityA();

        BookEntity bookEntityA = TestDataUtil.createTestBookEntityA(authorEntityA);
        bookEntityA.setAuthorEntity(authorEntityA);

        underTest.save(bookEntityA);

        bookEntityA.setTitle("UPDATE");

        underTest.save(bookEntityA);

        Optional<BookEntity> result = underTest.findById(bookEntityA.getIsbn());

        assertThat(result).isPresent();

        assertThat(result.get()).isEqualTo(bookEntityA);
    }

    @Test
    public void testThatAuthorsCanBeDeleted() {
        AuthorEntity authorEntityA = TestDataUtil.createTestAuthorEntityA();
        authorDao.save(authorEntityA);

        BookEntity bookEntityA = TestDataUtil.createTestBookEntityA(authorEntityA);

        underTest.save(bookEntityA);

        underTest.deleteById(bookEntityA.getIsbn());

        Optional<BookEntity> result = underTest.findById(bookEntityA.getIsbn());

        assertThat(result).isEmpty();
    }
}
