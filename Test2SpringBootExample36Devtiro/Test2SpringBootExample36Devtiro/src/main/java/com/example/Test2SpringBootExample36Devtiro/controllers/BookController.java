package com.example.Test2SpringBootExample36Devtiro.controllers;

import com.example.Test2SpringBootExample36Devtiro.domain.dto.BookDto;
import com.example.Test2SpringBootExample36Devtiro.domain.entities.BookEntity;
import com.example.Test2SpringBootExample36Devtiro.mappers.Mapper;
import com.example.Test2SpringBootExample36Devtiro.services.BookService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
public class BookController {
    private Mapper<BookEntity, BookDto> bookMapper;

    private BookService bookService;

    public BookController(Mapper<BookEntity, BookDto> bookMapper, BookService bookService) {
        this.bookMapper = bookMapper;
        this.bookService = bookService;
    }

    @PutMapping("/books/{isbn}")
    public ResponseEntity<BookDto> createUpdateBook(
        @PathVariable("isbn") String isbn,
        @RequestBody BookDto bookDto
    ) {
        BookEntity bookEntity = bookMapper.mapFrom(bookDto);

        boolean bookExists = bookService.isExists(isbn);

        BookEntity savedBookEntity = bookService.createUpdateBook(isbn, bookEntity);

        BookDto savedBookDto = bookMapper.mapTo(savedBookEntity);

        if(bookExists) {
            return new ResponseEntity<>(savedBookDto, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(savedBookDto, HttpStatus.CREATED);
        }
    }

//    @GetMapping(path = "/books")
//    public List<BookDto> listAuthors() {
//        List<BookEntity> books = bookService.findAll();
//
//        return books.stream()
//                .map(bookMapper::mapTo)
//                .collect(Collectors.toList());
//    }

    @GetMapping(path = "/books")
    public Page<BookDto> listAuthors(Pageable pageable) {
        Page<BookEntity> books = bookService.findAll(pageable);

        return books.map(bookMapper::mapTo);
    }

    @GetMapping(path = "/books/{isbn}")
    public ResponseEntity<BookDto> getBook(
            @PathVariable("isbn") String isbn
    ) {
        Optional<BookEntity> foundBook = bookService.findOne(isbn);

        return  foundBook.map(
                bookEntity -> {
                    BookDto bookDto = bookMapper.mapTo(bookEntity);

                    return new ResponseEntity<>(bookDto, HttpStatus.OK);
                }
        ).orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PatchMapping(path = "/books/{isbn}")
    public ResponseEntity<BookDto> partialUpdateBook(
            @RequestBody BookDto bookDto,
            @PathVariable("isbn") String isbn
    ) {
        if(!bookService.isExists(isbn)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        BookEntity bookEntity = bookMapper.mapFrom(bookDto);

        BookEntity updatedBookEntity = bookService.partialUpdate(isbn, bookEntity);

        return new ResponseEntity<>(
                bookMapper.mapTo(updatedBookEntity),
                HttpStatus.OK
        );
    }

    @DeleteMapping(path = "/books/{isbn}")
    public ResponseEntity deleteAuthor(
            @PathVariable("isbn") String isbn
    ) {
        bookService.delete(isbn);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
