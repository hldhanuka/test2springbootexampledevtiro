package com.example.Test2SpringBootExample36Devtiro.mappers.impl;

import com.example.Test2SpringBootExample36Devtiro.domain.dto.AuthorDto;
import com.example.Test2SpringBootExample36Devtiro.domain.entities.AuthorEntity;
import com.example.Test2SpringBootExample36Devtiro.mappers.Mapper;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class AuthorMapperImpl implements Mapper<AuthorEntity, AuthorDto> {
    private ModelMapper modelMapper;

    public AuthorMapperImpl(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public AuthorDto mapTo(AuthorEntity authorEntity) {
        return modelMapper.map(authorEntity, AuthorDto.class);
    }

    @Override
    public AuthorEntity mapFrom(AuthorDto authorDto) {
        return modelMapper.map(authorDto, AuthorEntity.class);
    }
}
