package com.example.Test2SpringBootExample36Devtiro.mappers;

public interface Mapper<A, B> {
    B mapTo(A a);

    A mapFrom(B b);
}
