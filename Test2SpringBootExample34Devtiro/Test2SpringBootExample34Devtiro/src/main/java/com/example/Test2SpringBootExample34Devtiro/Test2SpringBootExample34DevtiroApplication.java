package com.example.Test2SpringBootExample34Devtiro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Test2SpringBootExample34DevtiroApplication {

	public static void main(String[] args) {
		SpringApplication.run(Test2SpringBootExample34DevtiroApplication.class, args);
	}

}
