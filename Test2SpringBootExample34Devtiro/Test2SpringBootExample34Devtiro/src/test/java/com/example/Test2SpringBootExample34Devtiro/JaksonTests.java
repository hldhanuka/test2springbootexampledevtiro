package com.example.Test2SpringBootExample34Devtiro;

import com.example.Test2SpringBootExample34Devtiro.domain.Book;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class JaksonTests {
    @Test
    public void testThatObjectMapperCanCreateJsonFromJavaObject() throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();

        Book book = Book.builder()
                .isbn("993-34393-003")
                .title("test")
                .author("Dhanuka")
                .yearPublished("2005")
                .build();

        String result = objectMapper.writeValueAsString(book);

        assertThat(result).isEqualTo("{\"isbn\":\"993-34393-003\",\"title\":\"test\",\"author\":\"Dhanuka\",\"year\":\"2005\"}");
    }

    @Test
    public void testThatObjectMapperCanCreateJavaObjectFromJsonObject() throws JsonProcessingException {
        Book book = Book.builder()
                .isbn("993-34393-003")
                .title("test")
                .author("Dhanuka")
                .yearPublished("2005")
                .build();

        String json = "{\"test\":\"test\",\"isbn\":\"993-34393-003\",\"title\":\"test\",\"author\":\"Dhanuka\",\"year\":\"2005\"}";

        ObjectMapper objectMapper = new ObjectMapper();

        Book result = objectMapper.readValue(json, Book.class);

        assertThat(result).isEqualTo(book);
    }
}
