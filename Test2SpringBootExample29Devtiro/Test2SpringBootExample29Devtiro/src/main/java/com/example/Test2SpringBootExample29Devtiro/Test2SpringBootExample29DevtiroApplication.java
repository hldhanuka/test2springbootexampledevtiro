package com.example.Test2SpringBootExample29Devtiro;

import com.example.Test2SpringBootExample29Devtiro.config.PizzaConfig;
import lombok.extern.java.Log;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Log
public class Test2SpringBootExample29DevtiroApplication implements CommandLineRunner {
	private PizzaConfig pizzaConfig;

	public Test2SpringBootExample29DevtiroApplication(PizzaConfig pizzaConfig) {
		this.pizzaConfig = pizzaConfig;
	}

	public static void main(String[] args) {
		SpringApplication.run(Test2SpringBootExample29DevtiroApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		log.info(
				String.format("I want a %s crust pizza, with %s and %s sause",
				pizzaConfig.getCurst(),
				pizzaConfig.getTapping(),
				pizzaConfig.getSauce()
			)
		);
	}
}
