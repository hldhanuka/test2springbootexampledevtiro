package com.example.Test2SpringBootExample27Devtiro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Test2SpringBootExample27DevtiroApplication {

	public static void main(String[] args) {
		SpringApplication.run(Test2SpringBootExample27DevtiroApplication.class, args);
	}

}
