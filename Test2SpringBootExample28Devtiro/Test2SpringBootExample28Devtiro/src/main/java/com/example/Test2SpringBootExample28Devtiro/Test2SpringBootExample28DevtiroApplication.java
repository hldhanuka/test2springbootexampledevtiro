package com.example.Test2SpringBootExample28Devtiro;

import com.example.Test2SpringBootExample28Devtiro.service.ColourPrinter;
import lombok.extern.java.Log;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Log
public class Test2SpringBootExample28DevtiroApplication implements CommandLineRunner {
	private ColourPrinter colourPrinter;

	public Test2SpringBootExample28DevtiroApplication(ColourPrinter colourPrinter) {
		this.colourPrinter = colourPrinter;
	}

	public static void main(String[] args) {
		SpringApplication.run(Test2SpringBootExample28DevtiroApplication.class, args);
	}

	@Override
	public void run(final String... args) {
		log.info(colourPrinter.print());
	}
}
