package com.example.Test2SpringBootExample28Devtiro.service.impl;

import com.example.Test2SpringBootExample28Devtiro.service.GreenPrinter;
import org.springframework.stereotype.Component;

public class EnglishGreenPrinter implements GreenPrinter {
    @Override
    public String print() {
        return "green";
    }
}
