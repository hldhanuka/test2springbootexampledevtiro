package com.example.Test2SpringBootExample28Devtiro.service.impl;

import com.example.Test2SpringBootExample28Devtiro.service.BluePrinter;
import com.example.Test2SpringBootExample28Devtiro.service.ColourPrinter;
import com.example.Test2SpringBootExample28Devtiro.service.GreenPrinter;
import com.example.Test2SpringBootExample28Devtiro.service.RedPrinter;
import org.springframework.stereotype.Component;

@Component
public class ColourPrinterImpl implements ColourPrinter {
    private RedPrinter redPrinter;

    private BluePrinter bluePrinter;

    private GreenPrinter greenPrinter;

    public ColourPrinterImpl(
            RedPrinter redPrinter,
            BluePrinter bluePrinter,
            GreenPrinter greenPrinter
    ) {
        this.redPrinter = redPrinter;
        this.bluePrinter = bluePrinter;
        this.greenPrinter = greenPrinter;
    }

    @Override
    public String print() {
        return String.join(
                ",",
                redPrinter.print(),
                bluePrinter.print(),
                greenPrinter.print()
        );
    }
}
