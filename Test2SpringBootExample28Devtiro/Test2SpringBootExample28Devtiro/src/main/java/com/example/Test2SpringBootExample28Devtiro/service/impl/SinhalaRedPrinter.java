package com.example.Test2SpringBootExample28Devtiro.service.impl;

import com.example.Test2SpringBootExample28Devtiro.service.RedPrinter;
import org.springframework.stereotype.Component;

@Component
public class SinhalaRedPrinter implements RedPrinter {
    @Override
    public String print() {
        return "Rathu";
    }
}
