package com.example.Test2SpringBootExample28Devtiro.service.impl;

import com.example.Test2SpringBootExample28Devtiro.service.RedPrinter;
import org.springframework.stereotype.Component;

public class EnglishRedPrinter implements RedPrinter {
    @Override
    public String print() {
        return "red";
    }
}
