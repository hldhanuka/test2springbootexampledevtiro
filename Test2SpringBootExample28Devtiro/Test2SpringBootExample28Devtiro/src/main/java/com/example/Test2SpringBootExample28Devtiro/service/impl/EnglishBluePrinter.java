package com.example.Test2SpringBootExample28Devtiro.service.impl;

import com.example.Test2SpringBootExample28Devtiro.service.BluePrinter;
import org.springframework.stereotype.Component;

public class EnglishBluePrinter implements BluePrinter {
    @Override
    public String print() {
        return "blue";
    }
}
