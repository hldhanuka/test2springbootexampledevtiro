package com.example.Test2SpringBootExample31Devtiro;

import lombok.extern.java.Log;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

@SpringBootApplication
@Log
public class Test2SpringBootExample31DevtiroApplication implements CommandLineRunner {
	private final DataSource dataSource;

	public Test2SpringBootExample31DevtiroApplication(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public static void main(String[] args) {
		SpringApplication.run(Test2SpringBootExample31DevtiroApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		log.info("Data Source :- " + this.dataSource.toString());

		final JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

		jdbcTemplate.execute("select 1");
	}
}
